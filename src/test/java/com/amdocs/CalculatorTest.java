package com.amdocs;
import com.amdocs.Calculator;
import static org.junit.Assert.*;
import org.junit.Test;

public class CalculatorTest {
    @Test
    public void testAdd() throws Exception {

        final int kalk = new Calculator().add();
        assertEquals("Add", 9, kalk);
        
    }
    @Test   
    public void testSub() throws Exception {

        final int kalk = new Calculator().sub();
        assertEquals("Sub", 3, kalk);

    }
}

