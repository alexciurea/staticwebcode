package com.amdocs;

public class Increment {
	
	private static int counter = 1;
	//private int abc;
	
	public Increment() {
		this.counter = 1;	
	}
	  
	public int getCounter() {
	    return counter++;
	}
			
	public int decreasecounter(final int input) {
		if (input == 0){
	          counter--;
		}
		else {
			if (input != 1) {
			  counter++;
			}
		}
		return counter;
	}			
}
